import Vue from "vue";
import Router from "vue-router";
import About from "./components/About.vue";
import Home from "./components/Home.vue";

Vue.use(Router);

export default new Router({
  linkExactActiveClass: "active",
  routes: [
    {
      path: "/About",
      name: "about",
      component: About,
    },
    {
      path: "/",
      name: "home",
      component: Home,
    },
  ],
});
